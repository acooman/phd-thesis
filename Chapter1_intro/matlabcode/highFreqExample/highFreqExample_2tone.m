% in this script, I run a two-tone simulation on the high-frequency example
clear all
close all
clc

% create a two-tone multisine source
MSdef = MScreate( 1e6 , 2.4e9 ,'mode','bandpass','numTones',3);
MSdef.ampl(2)=0;
MSdef.MSnode=  'ref';

AC = ADSsimulateAC('highFreqExample.net',MSdef,'Start',2.4e9-2e9,'Stop',2.4e9+2e9,'Step',10e6);

figure(3)
plot(AC.freq,db(AC.out));
grid on

%%

amplList = [logspace(-3,log10(0.02),10) 0.04:0.02:1.1];

for ii=1:length(amplList)
    % set the amplitude of the two-tone to the wanted value
    MSdef.ampl([1 3]) = amplList(ii); 
    
    % simulate the whole thing with HB
    SPEC(ii) = ADSsimulateMS('highFreqExample.net',MSdef,'simulator','HB','numberOfRealisations',1,'outputNestLevel',2);
end

%% gather the correct bins

Exbin1 = MSdef.grid(1);
Exbin2 = MSdef.grid(3);

IM2bin1 = Exbin1+Exbin2;
IM2bin2 = Exbin2-Exbin1;

IM3bin1 = 2*Exbin1-Exbin2;
IM3bin2 = 2*Exbin2-Exbin1;

IM4bin1 = 2*Exbin1+2*Exbin2;
IM4bin2 = 2*Exbin2-2*Exbin1;

IM5bin1 = 3*Exbin2-2*Exbin1;
IM5bin2 = 3*Exbin1-2*Exbin2;

Fund = zeros( length(amplList),2 );
IM2  = zeros( length(amplList),2 );
IM3  = zeros( length(amplList),2 );
IM4  = zeros( length(amplList),2 );
IM5  = zeros( length(amplList),2 );
for ii=1:length(amplList);
    Fund(ii,:) = [SPEC(ii).out(findHBbin(Exbin1,SPEC(ii).freq)) SPEC(ii).out(findHBbin(Exbin2,SPEC(ii).freq))];
%     IM2(ii,:) =  [SPEC(ii).out(findHBbin(IM2bin1,SPEC(ii).freq)) SPEC(ii).out(findHBbin(IM2bin2,SPEC(11).freq))];
    IM3(ii,:) =  [SPEC(ii).out(findHBbin(IM3bin1,SPEC(ii).freq)) SPEC(ii).out(findHBbin(IM3bin2,SPEC(ii).freq))];
%     IM4(ii,:) =  [SPEC(ii).out(findHBbin(IM4bin1,SPEC(ii).freq)) SPEC(ii).out(findHBbin(IM4bin2,SPEC(11).freq))];
    IM5(ii,:) =  [SPEC(ii).out(findHBbin(IM5bin1,SPEC(ii).freq)) SPEC(ii).out(findHBbin(IM5bin2,SPEC(ii).freq))];
end


fun = @db;
figure(1)
clf
plot(fun(amplList),fun(Fund),'k');
hold on
% plot(db(amplList),db(IM2),'b');
plot(fun(amplList),fun(IM3),'r');
% plot(db(amplList),db(IM4),'c');
plot(fun(amplList),fun(IM5),'m');
grid on


% add the slope lines to extrapolate to the IP3 and IP5
attachpoint = 3;


% draw the linear line y=ax
plot(fun(amplList),fun(Fund(attachpoint,1)/amplList(attachpoint)*amplList),'k--');
plot(fun(amplList),fun(IM3(attachpoint,1)/amplList(attachpoint)^3*(amplList).^3),'r--');
plot(fun(amplList),fun(IM5(attachpoint,1)/amplList(attachpoint)^5*(amplList).^5),'m--');
%(IM3(attachpoint,1)/amplList(attachpoint)*

% make a plot for the 1dB compression point
figure(8)
plot(fun(amplList),fun(abs(Fund(:,1)).'-abs(Fund(attachpoint,1)/amplList(attachpoint)*amplList)),'k--');
grid on
ylim([0 2])
% xlim([-20 24])
% ylim([-20 24])

% figure(3)
% plot(db(squeeze(SPEC(end).out)),'+');

