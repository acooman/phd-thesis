% in this script, I run a two-tone simulation on the low-frequency example
clear all
close all
clc

% create a two-tone multisine source
MSdef = MScreate(1,10e4,'mode','bandpass','numTones',3);
MSdef.ampl(2)=0;
MSdef.MSnode=  'ref';

AC = ADSsimulateAC('lowFreqExample.net',MSdef,'Start',1,'Stop',10e9,'Dec',40);

figure(3)
semilogx(AC.freq,db(AC.out));
hold on
semilogx(AC.freq,db(AC.amp));
grid on

%%

amplList = [logspace(-2,log10(0.1),10) 0.2:0.1:3];

for ii=1:length(amplList)
    % set the amplitude of the two-tone to the wanted value
    MSdef.ampl([1 3]) = amplList(ii); 
    
    % simulate the whole thing with HB
    SPEC(ii) = ADSsimulateMS('lowFreqExample.net',MSdef,'simulator','HB','numberOfRealisations',1);
end



%% gather the correct bins

Exbin1 = MSdef.grid(1);
Exbin2 = MSdef.grid(3);

IM2bin1 = Exbin1+Exbin2;
IM2bin2 = Exbin2-Exbin1;

IM3bin1 = 2*Exbin1-Exbin2;
IM3bin2 = 2*Exbin2-Exbin1;

IM4bin1 = 2*Exbin1+2*Exbin2;
IM4bin2 = 2*Exbin2-2*Exbin1;

IM5bin1 = 3*Exbin2-2*Exbin1;
IM5bin2 = 3*Exbin1-2*Exbin2;

Fund = zeros( length(amplList),2 );
IM2  = zeros( length(amplList),2 );
IM3  = zeros( length(amplList),2 );
IM4  = zeros( length(amplList),2 );
IM5  = zeros( length(amplList),2 );
for ii=1:length(amplList);
    Fund(ii,:) = [SPEC(ii).out(findHBbin(Exbin1,SPEC(ii).freq)) SPEC(ii).out(findHBbin(Exbin2,SPEC(ii).freq))];
%     IM2(ii,:) =  [SPEC(ii).out(findHBbin(IM2bin1,SPEC(ii).freq)) SPEC(ii).out(findHBbin(IM2bin2,SPEC(11).freq))];
    IM3(ii,:) =  [SPEC(ii).out(findHBbin(IM3bin1,SPEC(ii).freq)) SPEC(ii).out(findHBbin(IM3bin2,SPEC(11).freq))];
%     IM4(ii,:) =  [SPEC(ii).out(findHBbin(IM4bin1,SPEC(ii).freq)) SPEC(ii).out(findHBbin(IM4bin2,SPEC(11).freq))];
    IM5(ii,:) =  [SPEC(ii).out(findHBbin(IM5bin1,SPEC(ii).freq)) SPEC(ii).out(findHBbin(IM5bin2,SPEC(11).freq))];
end

figure(1)
clf
plot(db(amplList),db(Fund),'k');
hold on
% plot(db(amplList),db(IM2),'b');
plot(db(amplList),db(IM3),'r');
% plot(db(amplList),db(IM4),'c');
plot(db(amplList),db(IM5),'m');
grid on
% xlim([-20 24])
% ylim([-20 24])


%%


% check the result
% plot(SPEC(end).freq,   db(squeeze(SPEC(end).out)+eps),'+');