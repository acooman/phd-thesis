function plotHTFs(HTFs,freq,varargin)
% 


[numHTFs,F]=size(HTFs);
numHTFs = (numHTFs-1)/2;

if ~exist('freq','var')
    freq = 1:F;
else
    if isempty(freq)
        freq = 1:F;
    else
        if length(freq)~=F
            error('freq vector should have the same length as the HTFs vector');
        end
    end
end




for hh = -numHTFs:numHTFs
    plot3(hh*ones(F,1),freq,HTFs(hh+numHTFs+1,:),varargin{:});
    if hh==-numHTFs
        hold on
    end
end