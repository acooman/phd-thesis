clear variables
close all
clc

HB = ADSimportSimData('choose_NL_order.mat');

% moving the baseline trick doesn't work with matlab2tikz
% so I do everyting +400 and play with the tick labels

figure(1)
clf
subplot(121)
stem(HB.sim1_HB.freq/1e9,db(HB.sim1_HB.I_SRC1(1,:))+400,'+');
ylim([0 400]);
set(gca,'YTick',0:100:400,'YTickLabel',arrayfun(@num2str,-400:100:0,'uniformOutput',false));
xlabel('Harmonic number');
ylabel('Current $\left[ \mathrm{dBA} \right]$','interpreter','none');
title('$V_{in}=0.1\mathrm{V}$','interpreter','none','fontWeight','normal');
grid on
subplot(122)
stem(HB.sim1_HB.freq/1e9,db(HB.sim1_HB.I_SRC1(2,:))+400,'+');
ylim([0 400]);
set(gca,'YTick',0:100:400,'YTickLabel',arrayfun(@num2str,-400:100:0,'uniformOutput',false));
xlabel('Harmonic number');
title('$V_{in}=1\mathrm{V}$','interpreter','none','fontWeight','normal');
grid on


matlab2tikz('choose_NL_order.tex','width','\figurewidth','height','\figureheight','parseStrings',false);