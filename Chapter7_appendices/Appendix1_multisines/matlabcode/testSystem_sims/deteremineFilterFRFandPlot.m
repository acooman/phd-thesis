function deteremineFilterFRFandPlot( TRAN , FILTER_HB, FILTER_HB_freq,NAME,xlims,reducedensity)

if ~exist('NAME','var') 
    NAME = 'HB';
end


if ~exist('reducedensity','var') 
    reducedensity = false;
end

if ~exist('xlims','var')
    xlims = [8.5 11.5];
end
ylimsfilt = [-100 0];

% calculate the frequency response of the filter
fundBins_TRAN = find((TRAN.freq>(xlims(1)*1e9))&(TRAN.freq<(xlims(2)*1e9)));
FILTER_TRAN = squeeze(TRAN.out(:,end,fundBins_TRAN))./squeeze(TRAN.nl(:,end,fundBins_TRAN));

% in the transient simulation, also plot the full waveform
figure(101)
clf
if reducedensity
    plot(TRAN.freq(1:2:end)/1e9,db(squeeze(TRAN.nl(:,end,1:2:end))),'.','markersize',2);
else
    plot(TRAN.freq/1e9,db(squeeze(TRAN.nl(:,end,:))),'.','markersize',2);
end
title('$I_\mathrm{nl}$','Interpreter','none','FontWeight','normal')
xlabel('Frequency $\left[ \mathrm{GHz} \right]$','Interpreter','none')
ylabel('Amplitude $\left[ \mathrm{dBV} \right]$','Interpreter','none')
grid on;
xlim([0 max(TRAN.freq)/1e9]);

cleanfigure
matlab2tikz([NAME '_fullspec.tex'],'width','\figurewidth','height','\figureheight','parseStrings',false);


figure(3)
clf
subplot(121)
% plot the voltage at the internal node
plot(TRAN.freq(fundBins_TRAN)/1e9,db(squeeze(TRAN.nl(:,end,fundBins_TRAN))),'.','markersize',2);
ylim([-300 0])
xlim(xlims);
xlabel('Frequency $\left[ \mathrm{GHz} \right]$','Interpreter','none')
ylabel('Amplitude $\left[ \mathrm{dBV} \right]$','Interpreter','none')
set(gca,'YTick',[-300 -200 -100 0])
title('$I_\mathrm{nl}$','Interpreter','none','FontWeight','normal')
grid on
subplot(122)
% plot the voltage at the output node
plot(TRAN.freq(fundBins_TRAN)/1e9,db(squeeze(TRAN.out(:,end,fundBins_TRAN))),'.','markersize',2);
xlabel('Frequency $\left[ \mathrm{GHz} \right]$','Interpreter','none')
ylim([-300 0])
set(gca,'YTick',[-300 -200 -100 0],'YTickLabel',{})
xlim(xlims);
grid on
title('$V_\mathrm{out}$','Interpreter','none','FontWeight','normal')

cleanfigure
matlab2tikz([NAME '_inout.tex'],'width','\figurewidth','height','\figureheight','parseStrings',false);


figure(4)
clf
subplot(121)
plot(TRAN.freq(fundBins_TRAN)/1e9,db(FILTER_TRAN));
if nargin>1
hold on
plot(FILTER_HB_freq/1e9,db(FILTER_HB),'r--');
end
xlim(xlims);
% ylim([-100 0]);
set(gca,'YTick',[-100 -50 0])
grid on
xlabel('Frequency $\left[ \mathrm{GHz} \right]$');
ylabel('Magnitude $\left[ \mathrm{dB} \right]$')

subplot(122)
plot(TRAN.freq(fundBins_TRAN)/1e9,(angle(FILTER_TRAN)));
if nargin>1
hold on
plot(FILTER_HB_freq/1e9,(angle(FILTER_HB)),'r--');
end
xlim(xlims);
set(gca,'YTick',[-pi -0.5*pi 0 pi/2 pi],'YTickLabels',{'$-\pi$','$-0.5\pi$','$0$','$0.5\pi$','$0.5\pi$'})
ylim([-pi pi])
grid on
xlabel('Frequency $\left[ \mathrm{GHz} \right]$');
ylabel('Phase $\left[ \mathrm{rad} \right]$')

cleanfigure
matlab2tikz([NAME '_filter.tex'],'width','\figurewidth','height','\figureheight','parseStrings',false);


end

