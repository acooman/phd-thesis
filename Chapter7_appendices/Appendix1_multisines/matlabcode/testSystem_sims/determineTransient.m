function determineTransient(raw,P,envsim,NAME,fig)

if ~exist('fig','var')
    fig=true;
end

[~,errorTransient] = checkTransients(raw,P);



if envsim
    trans = squeeze(errorTransient.out(1,:,2)).';
    trans = trans(:);
else
    trans = squeeze(errorTransient.out).';
    trans = trans(:);
    trans = trans(1:100:end);
end

if fig
figure(2)
end
plot(linspace(0,P-1,length(trans)),log10(abs(trans)).');
set(gca,'XTick',0:P-1);
set(gca,'YTick',[-16 -12 -8 -4 0]);
ylim([-16 0])
grid on
ylabel('$\mathrm{log}_{10}\left(transient\right)$','Interpreter','none')
xlabel('Periods')

if fig
cleanfigure
matlab2tikz([NAME '_transient.tex'],'width','\figurewidth','height','\figureheight','parseStrings',false);
end

end