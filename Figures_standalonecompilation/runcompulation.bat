pdflatex --enable-write18 standalone_figures.tex

cd Figures
del *.glo
del *.acn
del *.dpth
del *.log
del *.xdy

cd ..