function [y, transient] = ReshapeExpData(data, varargin)
%
%   function [y, transient] = ReshapeExpData (data, varargin)
%
%   Reshapes simulation/experimental data to the wanted format. 
%   Possibilities:  - M x P x N     (default)
%                   - M x PN        (type = realisation)            
%                   - MPN           (if data is not a vector)
%   The transient data is also returned in the format M x T
%
%
%   Required input:
%       - data  = to be converted data
% 
%   Optional inputs: 
%       - M     = number of realisations
%       - P     = number of periods
%       - T     = transient relative to P
%       - N     = datapoints in one period
%       - type: - default: split in realisations and periods 
%               - realisation: group all periods together
%   
%   The inverse operation is also implemented by supplying a M x P x N
%   matrix to the function.
%   
%   Written by Dries Peumans
%   Date: 30/11/2015

%%  Input parsing

%   Initialisation
    p = inputParserEgon();
    p.FunctionName = 'ReshapeExpData';
    p.StructExpand = true;
    
%   Definitions
    p.addRequired('data')
    p.addParameter('M', 0, @isnumeric)  % Realisations
    p.addParameter('P', 0, @isnumeric)  % Periods
    p.addParameter('T', 0, @isnumeric)  % Transient
    p.addParameter('N', 0, @isnumeric)  % Samples
    p.addParameter('type', [], @ischar) % Reshape type
    
    % Parse the input
    args = p.parse(data, varargin{:});
    
%   Define valuable inputs
    in      = args.Results.data;    
    M       = args.Results.M;
    P       = args.Results.P;
    N       = args.Results.N;
    T       = args.Results.T;
    Type    = args.Results.type;
    Nt      = T*N;
    
%   Cleanup of variables
    clear p data varargin args
    
%   Check if inputs have the correct format
%   Row or column vector?
    if  isvector(in)
        %   Check if vector has correct size
        if  M*(P*N + T*N) ~= length(in)
            error('M, P, T or N do not have the correct value.')
        else
            processing = 'vector';
            if  strcmp(Type, 'realisation')
                N = P*N;
                P = 1;
            end
        end
    else
        processing = 'MPN';
    end
    
%%  Perform the conversion

    %   Convert the vector to M x P x N
    if  strcmp(processing, 'vector')
        % Convert to column vector
        in = in(:);
        % Perform the conversion
        transient = zeros(M, Nt);
        y = zeros(M, P, N);
        for ii = 1:M
            % Transient
            transient_index = 1:Nt;
            transient(ii, :) =  in(transient_index);
            in(transient_index) = [];
            % Periods
            for jj = 1:P
                y(ii, jj, :) = in(1:N);
                in(1:N) = [];
            end
        end
        
    %   Convert the matrix M x P x N to a vector
    elseif  strcmp(processing, 'MPN')
        [dM, dP, ~] = size(in);
        y = [];
        % Loop over realisation and periods
        for ii = 1:dM
            for jj = 1:dP
                temp = squeeze(in(ii,jj,:));
                temp = temp(:);
                y = [y; temp];
            end
        end
        transient = [];
    end
end