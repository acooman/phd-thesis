% Simulate the behavioural filter with a multisine excitation
% Written by: Dries Peumans
% Date: 25/04/2016

clc
close all
clear all

%% Variables

% PLL specifications
PhaseNoise  = -80;       % Uniform phase noise profile  [dBc]
PhNoiProfX  = [1e1 1e2 1e3 1e4  1e5   3e5  1e6 1e7 ];
PhNoiProfY  = [-50 -75 -90 -105 -110 -110 -125 -150]+30;
Kvco        = 400e6;     % VCO gain                     [Hz/V]
Fref        = 100e6;     % Frequency reference clock    [Hz]
Fvco        = 1.8e9;     % VCO free running frequency   [Hz]
Fpll        = 1e6;       % PLL bandwidth                [Hz]
Iqp         = 1e-6;      % Charge pump current          [A]
Ndiv        = Fvco/Fref; % Frequency divider value      [ ]

figure(808)
clf
semilogx(PhNoiProfX,PhNoiProfY);
hold on
xlabel('Offset [Hz]');
ylabel('Phase noise [dBc/Hz]');
grid on

% Design filter
Wpll    = Fpll * 2*pi;          % Angular frequency [rad/s]
alpha   = 4;                    % Spacing from zero to BW
beta    = 6;                    % BW to double pole
gamma   = 3;                    % R-ratio of fourth pole
wz      = Wpll/alpha;           % Zero position [rad/s]
wp      = Wpll*beta;            % Pole position [rad/s]
NN      = Ndiv / (Iqp * Kvco); 

% Filter components
Rz = Wpll * NN;             % [ohm]
Cz = alpha / ( Wpll * Rz ); % [F]
Cp = Cz / (alpha * beta);   % [F]
R4 = Rz / gamma;            % [ohm]
C4 = Cp * gamma;            % [F]

% Loop filter transfer function
B = [Rz*Cz  1];
A = [Rz*Cz*R4*C4*Cp  Rz*Cz*Cp+R4*C4*(Cz+Cp)  Cz+Cp 0];

% Simulation
f0      = 1e4;
fmax    = 1e6;
fs      = fmax*100;
ExcFreq = f0:f0:fmax;
N       = fs/f0;
P       = 1;
T       = 1;
M       = 500;
OddMS   = false;

ExcAmpl = interp1(log10(PhNoiProfX),PhNoiProfY,log10(ExcFreq));
semilogx(ExcFreq,ExcAmpl,'r+')

%% Simulink

% Load the model
model = 'PLL_ADAM';
load_system(model)

% Get configuration
cs = getActiveConfigSet(model);

% Solver Parameters
cs.set_param('StartTime', '0')                     % Start time
cs.set_param('StopTime', num2str(((P+T)*N-1)/fs,15))  % Stop time
cs.set_param('SolverType', 'Fixed-step')           % Type
cs.set_param('Solver', 'ode4')                     % Solver
cs.set_param('EnableConcurrentExecution', 'off')   % Show concurrent execution options
cs.set_param('FixedStep', num2str(1/fs))           % Periodic sample time constraint

% Perform the simulation
for ii = 1:M
    disp(ii);
    
    % Input multisine [rad]
    ms = GeneratePhaseNoise(N, fs, ExcFreq, ExcAmpl, 0, OddMS);
    
    % Input
    in.time = (0:(P+T)*N-1)/fs;
    in.signals.dimensions = 1;
    in.signals.values = repmat(ms.', P+T, 1);
    
    % Perform the simulation
    sim(model)
    
    if ii==1
        % look at all the variables
        S = whos();
        % if the variabel is a timeseries, save its name in a vector;
        signals={};
        for ss=1:length(S)
            if strcmp(S(ss).class,'timeseries');
                signals{end+1} = S(ss).name;
            end
        end
        % preallocate the time domain vectors
        TIME=struct();
        for ss=1:length(signals)
            TIME.(signals{ss}) = zeros(M, P, N);
        end
    end
    
    % Get rid of the transient
    for ss=1:length(signals)
        eval(['TIME.' signals{ss} '(ii,:,:) = ReshapeExpData(' signals{ss} '.Data, ''M'', 1, ''P'', P, ''T'', T, ''N'', N);']);
    end
end

% clean up a little
clear timeseries

% remove the positive part of the spectrum, nobody cares about that anyway
fields = fieldnames(TIME);
for ss=1:length(fields)
    SPEC.(fields{ss}) = fft(TIME.(fields{ss}),[],3)/size(TIME.(fields{ss}),3)/sqrt(f0);
    SPEC.(fields{ss}) = SPEC.(fields{ss})(:,:,1:end/2);
end

% make the frequency axis
SPEC.freq = (0:N-1)*f0;
SPEC.freq = SPEC.freq(1:end/2);

%% Show some of the signals

% don't plot all the realisations, this takes a while for large M
if M>=10
    Mrng = 1:10;
else
    Mrng = 1:M;
end

figure(1)
clf
subplot(211)
plot(in.time(1:N), squeeze(TIME.REF(Mrng, end, :)).', '.')
title('REF')
xlabel('Time [s]')
subplot(212)
plot(SPEC.freq,db(squeeze(SPEC.REF(Mrng, end, :)).'), '.')
xlabel('Frequency [Hz]');

figure(2)
clf
subplot(211)
plot(in.time(1:N), squeeze(TIME.VCO_out(Mrng, end, :)).', '.')
title('VCO output')
xlabel('Time [s]')
subplot(212)
plot(SPEC.freq,db(squeeze(SPEC.VCO_out(Mrng, end, :)).'), '.')
xlabel('Frequency [Hz]');

figure(3)
clf
subplot(211)
plot(in.time(1:N), squeeze(TIME.PFD_in(Mrng, end, :)).', '.')
title('PFD input')
xlabel('Time [s]')
subplot(212)
plot(SPEC.freq,db(squeeze(SPEC.PFD_in(Mrng, end, :)).'), '.')
xlabel('Frequency [Hz]');

figure(4)
clf
subplot(211)
plot(in.time(1:N), squeeze(TIME.PFD_out(Mrng, end, :)).', '.')
title('PFD output')
xlabel('Time [s]')
subplot(212)
plot(SPEC.freq,db(squeeze(SPEC.PFD_out(Mrng, end, :)).'), '.')
xlabel('Frequency [Hz]');


%% Determine the BLAs

exBins = find(squeeze(db(SPEC.REF(1, end, :)))>-200);

[BLAs,Y,U,exBins,CovYs] = calculateSISO_BLA(SPEC,'input',{'PFD_in','VCO_in'},'output',{'PFD_out','VCO_out'},'reference','REF','exBins',exBins);

% calculate the BLA from reference to the output
REF_OUT = calculateSISO_BLA(SPEC,'input','REF','output','VCO_out','reference','REF','exBins',exBins);

% estimate the response of the lowpass filter
LF = calculateSISO_BLA(SPEC,'input','LF_in','output','LF_out','reference','REF','exBins',exBins);
LF = LF.mean_interp;

% plot the BLAs and their uncertainty
figure(11)
clf
subplot(121)
hold on
plot(BLAs(1).freq/1e6,db(BLAs(1).mean),'Color',[0 0.7 0],'Linewidth',1);
plot(BLAs(1).freq/1e6,db(BLAs(1).stdNL),'k');
title('PFD')
grid on
xlabel('Frequency $\left[ \mathrm{MHz} \right]$','interpreter','none')
ylabel('Magnitude $\left[ \mathrm{dB} \right]$','interpreter','none')
legend({'$G^\mathrm{BLA}_\mathrm{VCO}$','std'},'interpreter','none')

subplot(122)
hold on
plot(BLAs(2).freq/1e6,db(BLAs(2).mean),'Color',[0 0.7 0],'Linewidth',1);
plot(BLAs(2).freq/1e6,db(BLAs(2).stdNL),'k');
title('VCO')
grid on
xlabel('Frequency $\left[ \mathrm{MHz} \right]$','interpreter','none')
ylabel('Magnitude $\left[ \mathrm{dB} \right]$','interpreter','none')
legend({'$G^\mathrm{BLA}_\mathrm{VCO}$','std'},'interpreter','none')
% figure(15)
% plot(db(BLAs(1).Ys))

cleanfigure
matlab2tikz('PLL_BLA.tex','width','\figurewidth','height','\figureheight','parseStrings',false);


%%
freq = BLAs(1).freq_interp;
F = length(freq);
% DIV is the feedback factor
DIV = 1/Ndiv;
% calculate the frequency response from the distortion sources to the output
BLA1 = BLAs(1).mean_interp;
BLA2 = BLAs(2).mean_interp;
LOOPGAIN = DIV.*LF.*BLA1.*BLA2;
T1 = LF.*BLA2./(LOOPGAIN+1);
T2 = 1./(LOOPGAIN+1);

figure(165468)
clf
semilogy(squeeze(real(CovYs(1,1,:))),'Color',[0 0.7 0])
hold on
semilogy(squeeze(real(CovYs(2,2,:))),'Color',[1 0.7 0])
semilogy(squeeze(real(CovYs(1,2,:))),'Color',[0 0.7 1])
semilogy(squeeze(imag(CovYs(1,2,:))),'--','Color',[0 0.7 1])
legend({'Cd11','Cd22','Re(Cd12)','Im(Cd12)'})


figure(1698)
clf
subplot(211)
plot(db(T1),'Color',[0 0.7 0]);
hold on
plot(db(T2),'Color',[1 0.7 0]);
legend({'T1','T2'})
subplot(212)
plot(angle(T1),'Color',[0 0.7 0]);
hold on
plot(angle(T2),'Color',[1 0.7 0]);
legend({'T1','T2'})

% figure(16584)
% clf
% subplot(211)
% hold on
% plot(freq,db(T1),'Color',[0 0.7 0]);
% plot(freq,db(T2),'Color',[1 0.7 0]);
% plot(freq,db(Tout(1,:)),'.','Color',[0 0.7 0]);
% plot(freq,db(Tout(2,:)),'.','Color',[1 0.7 0]);
% subplot(212)
% hold on
% plot(freq,angle(T1),'Color',[0 0.7 0]);
% plot(freq,angle(T2),'Color',[1 0.7 0]);
% plot(freq,angle(Tout(1,:)),'.','Color',[0 0.7 0]);
% plot(freq,angle(Tout(2,:)),'.','Color',[1 0.7 0]);

% calculate the actual contributions
CONTpfd =    real(conj(T1).*T1.*squeeze(CovYs(1,1,:)).');
CONTvco =    real(conj(T2).*T2.*squeeze(CovYs(2,2,:)).');
CONTcov =  2*real(conj(T2).*T1.*squeeze(CovYs(1,2,:)).');

figure(13)
clf
if OddMS
    subplot(121)
    hold on
    plot(freq(2:2:end)/1e6,(CONTpfd(2:2:end) ),'bs');
    plot(freq(2:2:end)/1e6,(CONTvco(2:2:end)),'b+');
    plot(freq(2:2:end)/1e6,(CONTcov(2:2:end)),'c+');
    % plot the sum of the contributions
    plot(freq(2:2:end)/1e6,(CONTpfd(2:2:end)+CONTvco(2:2:end)+CONTcov(2:2:end)),'k-');
    % plot the measured distortion level
    plot(SPEC.freq(3:2:max(exBins))/1e6  ,(Y(2).disto(3:2:max(exBins))).^2,'-','Linewidth',2,'Color',[0.5 0.5 0.5]);
    % legend('TOTAL','IN','OUT','Location','SE');
    xlabel('Frequency $\left[ \mathrm{MHz} \right]$','interpreter','none');
    % set(gca,'YTick',[-150 -140]);
    % set(gca,'XTick',[0 0.5 1]);
    grid on
    ylabel('Contributions $\left[ \mathrm{dBc} \right]$','interpreter','none');
    subplot(122)
    hold on
    plot(freq(1:2:end)/1e6,(CONTpfd(1:2:end) ),'rs');
    plot(freq(1:2:end)/1e6,(CONTvco(1:2:end)),'r+');
    plot(freq(1:2:end)/1e6,(CONTcov(1:2:end)),'m-');
    % plot the sum of contributions
    plot(freq(1:2:end)/1e6,(CONTpfd(1:2:end)+CONTvco(1:2:end)+CONTcov(1:2:end)),'k-');
    % plot the measured distortion level
    plot(SPEC.freq(2:2:max(exBins))/1e6  ,(Y(2).disto(2:2:max(exBins))).^2,'-','Linewidth',2,'Color',[0.5 0.5 0.5]);
    % legend('TOTAL','IN','OUT','Location','E');
    % set(gca,'YTick',[-200 -80],'YAxisLocation','right');
    xlabel('Frequency $\left[ \mathrm{MHz} \right]$','interpreter','none');
    % set(gca,'XTick',[0 0.5 1]);
    grid on
else
    hold on
    plot( freq/1e6 , CONTpfd , '-' ,'Color',[0 0.7 0],'Linewidth',2);
    plot( freq/1e6 , CONTvco , '-' ,'Color',[1 0.7 0],'Linewidth',2);
    plot( freq/1e6 , CONTcov , '-' ,'Color',[0 0.7 1],'Linewidth',2);
    % plot the sum of the contributions
    plot( freq/1e6 , CONTpfd+CONTvco+CONTcov ,'k-','Linewidth',2);
    % plot the measured distortion level
    plot(SPEC.freq(1:max(exBins))/1e6  ,(Y(2).disto(1:max(exBins))).^2,'-','Linewidth',2,'Color',[0.5 0.5 0.5]);
    legend({'PFD','VCO','COV','SUM','DISTO'},'Location','SE');
    xlabel('Frequency $\left[ \mathrm{MHz} \right]$','interpreter','none');
    % set(gca,'YTick',[-150 -140]);
    % set(gca,'XTick',[0 0.5 1]);
    grid on
    ylabel('Contributions $\left[ \mathrm{dBc} \right]$','interpreter','none');
    
end

% cleanfigure
% matlab2tikz('PLL_contribs.tex','height','\figureheight','width','\figurewidth','parseStrings',false)


%% generate the plots for the paper

if M>=10
    Mrng = 1:10;
else
    Mrng = 1:M;
end

maxBin = 3*exBins(end);


% correct the output spectrum with the input spectrum
INBAND = zeros(length(Mrng),1,length(exBins));
for mm=1:length(Mrng)
    INBAND(mm,1,:) = squeeze(SPEC.VCO_out(Mrng(mm),1,exBins)).' - squeeze(SPEC.REF(Mrng(mm),1,exBins)).' .* REF_OUT.mean;
end

% show the reference signal and the output signal of the VCO
figure(506)
clf
subplot(121)
plot(SPEC.freq(exBins)/1e6,db(squeeze(SPEC.REF(1, end, exBins)).'), 'k.','MarkerSize',1);
xlim([0 SPEC.freq(maxBin)]/1e6);
ylim([-160 -40])
xlabel('Frequency $\left[ \mathrm{MHz} \right]$','interpreter','none')
% ylabel('Phase domain signal $\left[ \mathrm{dBc} \right]$','interpreter','none')
grid on
set(gca,'YTick',[-120 -80 -40 0]);

subplot(122)
plot(SPEC.freq(exBins)/1e6,db(squeeze(SPEC.VCO_out(Mrng, end, exBins)).'), 'k.','MarkerSize',1);
hold on
plot(SPEC.freq(exBins)/1e6,db(squeeze(INBAND)),'r.','MarkerSize',1);
plot(SPEC.freq(1:2:maxBin)/1e6,db(squeeze(SPEC.VCO_out(Mrng, end, 1:2:maxBin)).'), 'b.','MarkerSize',1);
plot(SPEC.freq(exBins(end)+2:2:maxBin)/1e6,db(squeeze(SPEC.VCO_out(Mrng, end, exBins(end)+2:2:maxBin)).'), 'r.','MarkerSize',1);
xlim([0 SPEC.freq(maxBin)]/1e6);
ylim([-160 -40])
xlabel('Frequency $\left[ \mathrm{MHz} \right]$','interpreter','none')
grid on
set(gca,'YTick',[-120 -80 -40 0],'YTickLabel',{});

cleanfigure
matlab2tikz('PLL_disto.tex','height','\figureheight','width','\figurewidth','parseStrings',false)

%% check whether the SIMO BLA is well predicted

REFAMP = squeeze(abs(SPEC.REF(1,1,exBins))).';

figure(18320)
clf
subplot(131)
hold on
plot(BLAs(1).freq , db(U(1).mean));
plot(BLAs(1).freq , db(REFAMP./(LOOPGAIN+1)),'+');

subplot(132)
hold on
plot(BLAs(1).freq , db(Y(1).mean));
plot(BLAs(1).freq , db(REFAMP.*BLA1./(LOOPGAIN+1)),'+');

subplot(133)
hold on
plot(BLAs(1).freq , db(Y(2).mean));
plot(BLAs(1).freq , db(REFAMP.*BLA1.*LF.*BLA2./(LOOPGAIN+1)),'+');






