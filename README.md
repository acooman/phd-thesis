# Phd-Thesis



## How to compile

I wrote the whole thesis in Lyx. You will need at least lyx version 2.2 to be able to open and to compile the Thesis.

The git folder contains two Lyx modules:

1. `ADAMexample.module` creates the `\example` environment.

2. `ADAMGLOSSARIES.module` creates the commands related to the glossaries package, like `\gls` and `\glspl`

Lyx will complain when these modules are not added to Lyx. To be able to compile the thesis, you need to copy the modules to `C:\Users\yourusername\AppData\Roaming\LyX2.2\layouts`.

After copying, you need to reconfigure Lyx (Tools -> Reconfigure)
