clc
clear variables
close all

set(0,'DefaultTextInterpreter','none');

addpath(genpath('C:\Users\Adam\Google Drive\Notities\Stability Hardy\Code Fabien\'));

RES = ADSimportSimData('BALamp_coarse.mat');

Z = RES.sim1_AC.coll1;
f = RES.sim1_AC.freq;

[ystable,yunstable,fs,fus,poles,freq,Z_filtered] = StableUnstableProjection(Z,f,'plot',false);


figure(1)
clf
subplot(121)
hold on
plot(f/1e9,db(Z),'Color',[0 100/255 0],'linewidth',1);
plot(freq/1e9,db(ystable),'b','linewidth',1)
plot(freq/1e9,db(yunstable),'r','linewidth',1)
xlim([0 max(f)]/1e9)
ylim([-10 60]);
set(gca,'YTick',[0 20 40 60])
set(gca,'XTick',0:10:50)
xlabel('Frequency $\left[ \mathrm{GHz} \right]$');
ylabel('Magnitude $\left[ \mathrm{dB}\left( \Omega \right) \right]$','interpreter','none');
grid on
legend({'$Z$','$Z_\mathrm{stable}$','$Z_\mathrm{unstable}$'},'interpreter','none');

subplot(122)
hold on
plot(f/1e9,unwrap(angle(Z))/pi,'Color',[0 100/255 0],'linewidth',1);%/pi
plot(freq/1e9,unwrap(angle(ystable))/pi,'b','linewidth',1)% /pi
plot(freq/1e9,unwrap(angle(yunstable))/pi,'r','linewidth',1) %/pi
xlim([0 max(f)]/1e9);
ylim([-6 2.5]);
ylabel('Phase $\left[ \mathrm{rad} \right]$','interpreter','none');
xlabel('Frequency $\left[ \mathrm{GHz} \right]$','interpreter','none');
set(gca,'XTick',0:10:50)
set(gca,'YTick',[-6 -4 -2 0 2],'YTickLabel',{'$-6\pi$','$-4\pi$','$-2\pi$','$-0\pi$','$2\pi$'})
grid on

cleanfigure
matlab2tikz('BalAmp_coarse.tex','width','\figurewidth','height','\figureheight','parseStrings',false);

%% do the same thing, but use the fine data

RES = ADSimportSimData('BALamp_fine.mat');

Z = RES.sim1_AC.coll1;
f = RES.sim1_AC.freq;

[ystable,yunstable,fs,fus,poles,freq,Z_filtered] = StableUnstableProjection(Z,f,'plot',false);

figure(2)
clf
subplot(121)
hold on
plot(f/1e9,db(Z),'Color',[0 100/255 0],'linewidth',1);
plot(freq/1e9,db(ystable),'b','linewidth',1);
plot(freq/1e9,db(yunstable),'r','linewidth',1);
xlim([0 max(f)]/1e9)
ylim([-10 60]);
set(gca,'YTick',[0 20 40 60])
set(gca,'XTick',0:10:50)
xlabel('Frequency $\left[ \mathrm{GHz} \right]$');
ylabel('Magnitude $\left[ \mathrm{dB}\left( \Omega \right) \right]$','interpreter','none');
grid on
legend({'$Z$','$Z_\mathrm{stable}$','$Z_\mathrm{unstable}$'},'interpreter','none');

subplot(122)
hold on
plot(f/1e9,unwrap(angle(Z))/pi,'Color',[0 100/255 0],'linewidth',1);
plot(freq/1e9,unwrap(angle(ystable))/pi,'b','linewidth',1);
plot(freq/1e9,unwrap(angle(yunstable))/pi,'r','linewidth',1);
xlim([0 max(f)]/1e9);
ylim([-6 2.5]);
ylabel('Phase $\left[ \mathrm{rad} \right]$','interpreter','none');
xlabel('Frequency $\left[ \mathrm{GHz} \right]$','interpreter','none');
set(gca,'XTick',0:10:50)
set(gca,'YTick',[-6 -4 -2 0 2],'YTickLabel',{'$-6\pi$','$-4\pi$','$-2\pi$','$-0\pi$','$\pi$'})
grid on

cleanfigure
matlab2tikz('BalAmp_fine.tex','width','\figurewidth','height','\figureheight','parseStrings',false);


%%
% clc
% 
% 
% figure(1)
% ToPlot = [1];
% clf
% plot(freq/1e9,db(StabRes.Z_filtered(ToPlot,:)),'-','Color',[0 100/255 0]);
% hold on
% plot(StabRes.freq/1e9,db(StabRes.ystable(ToPlot,:)),'b--')
% plot(StabRes2.freq/1e9,db(StabRes2.yunstable(ToPlot,:)),'-','Color',[1 150/255 0])
% plot(StabRes.freq/1e9,db(StabRes.yunstable(ToPlot,:)),'r-.');
% ylim([-10 50])
% xlim([0 50])
% ylabel('Impedance $\left[ \mathrm{dB} (\Omega) \right]$','interpreter','none')
% xlabel('Frequency $\left[ \mathrm{GHz} \right]$','interpreter','none')
% set(gca,'XTick',[ 1.3 10 20 30 40 50]);
% set(gca,'YTick',[0 30 50]);
% grid on
% cleanfigure
% matlab2tikz('..\..\pics\BalAmp_projection.tex','width','\figurewidth','height','\figureheight','parseStrings',false);


