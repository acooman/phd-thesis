% in this script I play with the filter in the stable/unstable projection
% to look at its influence
clear all
close all
clc

addpath(genpath('C:\users\adam.cooman\Google Drive\Notities\Stability Hardy\Code Fabien\'));

% generate the data

% datasource = 'stable_ex.mat';
datasource = 'stable_ex.mat';

if ~strcmpi(datasource,'NEW');
    load(datasource);
    f = freq;
else
    % N is the amount of stable poles in the random system
    N = 50;
    % I generate a random system without a perfect integrator
    p={0};
    while any(db(p{1})<-200)
        % generate a random system of very high order
        sys = rss(N);
        % check whether there's a perfect integrator present in the system,
        % if there is, just retry
        [z,p,k] = zpkdata(sys);
    end
    % if you want an unstable system, set this variable to true
    unstable = true;
    if unstable
        unst_real = 0.5;     unst_imag = 3*2*pi;
        % add in the unstable poles. If the imaginary part is zero, just add a
        % single pole on the real axis
        if unst_imag==0
            p = {[p{1};unst_real]};
        else
            p = {[p{1};unst_real+unst_imag*1i;unst_real-unst_imag*1i]};
        end
        % and regenerate the system with the new poles
        sys = zpk(z,p,k);
    end
    % add some delay to the input and the output, to maks stuff more difficult
    sys.OutputDelay = 3*rand();
    sys.InputDelay = 3*rand();
    % plot the pole-zero map
    figure(3)
    pzmap(sys);
    % fmax is the maximum frequency simulated
    fmax = 5;
    % the comp frequency grid is compensated in advance such that a
    % linear grid is obtained on the unit disc. This avoids the need
    % for a linear interpolation to use the FFT
    % 2^N points on the unit circle will be used
    N = 18;
    theta = linspace(0,2*pi,2^N+1);
    theta = theta(1:end-1);
    theta = theta(end/2+1:end-end/8+1);
    e = exp(1i*theta);
    c = exp(1i*(2*pi-pi/4));
    c = fmax/(imag((c+1)./(c-1)));
    f = imag((e+1)./(e-1))*c;
    % it can happen that the first frequency point is negative,
    % we'll just fix that
    f(1) = abs(f(1));
    % get the frequency response of that system on different frequencies
    Z = squeeze(freqresp(sys,f,'Hz'));
    % set calcerror to true if you want to calculate the error
    calcError = false;
end


figure(10)
clf

Orders =  [4:14];

for ii=1:length(Orders)
    % perform the projection
    [ystable,yunstable,fs,fus,poles,freq,Z_filtered] = ...
        StableUnstableProjection(Z,f,'Filtertype','LOWPASS','FilterOrder',Orders(ii),...
        'plot',false,'estimatePoles',false);
    % plot the results
    figure(10)
    plot(freq,db(Z_filtered),'Color',[0 100/255 0]);
    if ii==1
        hold on
    end
    plot(freq,db(ystable),'b--','LineWidth',2);
    plot(freq,db(yunstable),'r-.');
end
ylim([-150 50])
xlim([0 max(f)])
xlabel('Frequency [Hz]');
ylabel('Magnitude [dB]');
set(gca,'xTick',[0 1 2 3 4 5]);


%% Also show that we can zoom in on an instability



