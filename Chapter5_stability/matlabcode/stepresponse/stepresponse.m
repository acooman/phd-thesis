% create a plot that shows how a stable system recovers from a kick
clear all
close all
clc

time = 0:0.01:10;

signal = zeros(size(time));
% find the 1 second bin
[~,ind]=min(abs(time-4));
% put the impulse at 1 second
signal(ind)=1;

% the system is a first order system
sys = zpk([],-1,1);
% transform to sampled time
sysd= c2d(sys,time(2));
[b,a]=tfdata(sysd);
% get the impulse response of the system
resp = 10*filter(b{1},a{1},signal);

% now create an unstable system
sysunst = zpk([],[1+2*pi*5i 1-2*pi*5i],1);
sysunstd = c2d(sysunst,time(2));
[b,a]=tfdata(sysunstd);
respunst=10*filter(b{1},a{1},10*signal);

figure(1)
clf
subplot(131)
plot(time,resp);
ylim([-1.5 1.5])
xlabel('Time')
set(gca,'XTick',[0 4 10],'XTickLabel','')
set(gca,'YTick',[-1 0 1],'YTickLabel','')
grid on
title('Stable orbit','FontWeight','normal')

subplot(132)
plot(time,sin(2*pi*5*time).*(1+resp));
xlabel('Time')
set(gca,'XTick',[0 4 10],'XTickLabel','')
set(gca,'YTick',[-1 0 1],'YTickLabel','')
grid on
ylim([-1.5 1.5])
title('Stable orbit','FontWeight','normal')

subplot(133)
plot(time,respunst,'r');
ylim([-1.5 1.5])
xlabel('Time')
set(gca,'XTick',[0 4 10],'XTickLabel','')
set(gca,'YTick',[-1 0 1],'YTickLabel','')
grid on
title('Unstable orbit','FontWeight','normal')

cleanfigure
matlab2tikz('stepresponse.tex','height','\figureheight','width','\figurewidth','parseStrings',false);

