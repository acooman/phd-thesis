#LyX 2.2 created this file. For more info see http://www.lyx.org/
\lyxformat 508
\begin_document
\begin_header
\save_transient_properties true
\origin unavailable
\textclass scrbook
\begin_preamble
\input{../config/preamble.tex}
\end_preamble
\use_default_options true
\begin_modules
ADAMexample
todonotes
ADAMGLOSSARIES
\end_modules
\maintain_unincluded_children false
\language british
\language_package default
\inputencoding auto
\fontencoding global
\font_roman "default" "default"
\font_sans "default" "default"
\font_typewriter "default" "default"
\font_math "auto" "auto"
\font_default_family default
\use_non_tex_fonts false
\font_sc false
\font_osf false
\font_sf_scale 100 100
\font_tt_scale 100 100
\graphics default
\default_output_format default
\output_sync 0
\bibtex_command default
\index_command default
\paperfontsize default
\spacing single
\use_hyperref true
\pdf_bookmarks true
\pdf_bookmarksnumbered false
\pdf_bookmarksopen false
\pdf_bookmarksopenlevel 1
\pdf_breaklinks false
\pdf_pdfborder true
\pdf_colorlinks false
\pdf_backref false
\pdf_pdfusetitle true
\papersize b5paper
\use_geometry false
\use_package amsmath 1
\use_package amssymb 1
\use_package cancel 1
\use_package esint 1
\use_package mathdots 1
\use_package mathtools 1
\use_package mhchem 1
\use_package stackrel 1
\use_package stmaryrd 1
\use_package undertilde 1
\cite_engine basic
\cite_engine_type default
\biblio_style plain
\use_bibtopic false
\use_indices false
\paperorientation portrait
\suppress_date false
\justification true
\use_refstyle 1
\index Index
\shortcut idx
\color #008000
\end_index
\secnumdepth 1
\tocdepth 3
\paragraph_separation skip
\defskip smallskip
\quotes_language english
\papercolumns 1
\papersides 2
\paperpagestyle default
\tracking_changes false
\output_changes false
\html_math_output 0
\html_css_as_file 0
\html_be_strict false
\end_header

\begin_body

\begin_layout Standard
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
setlength{
\backslash
figurewidth}{0.8
\backslash
columnwidth} 
\backslash
setlength{
\backslash
figureheight}{0.3
\backslash
columnwidth} 
\backslash
glsresetall
\end_layout

\end_inset


\end_layout

\begin_layout Chapter
Conclusions and Future work
\end_layout

\begin_layout Standard
\begin_inset VSpace vfill
\end_inset


\end_layout

\begin_layout Standard
In this final chapter of the thesis, we first summarise the results and
 detail the contributions made to both distortion analysis and stability
 analysis of analog electronic circuits.
 Then, we propose some possible further improvements and future research.
\end_layout

\begin_layout Standard
\begin_inset Newpage newpage
\end_inset


\end_layout

\begin_layout Subsection
Distortion Contribution Analysis
\end_layout

\begin_layout Standard
In the first part of this thesis, we introduced a technique to determine
 the distortion contributions in a circuit excited by a modulated signal.
 
\end_layout

\begin_layout Standard
As was shown in chapter
\begin_inset space ~
\end_inset


\begin_inset CommandInset ref
LatexCommand ref
reference "chap:Introduction"

\end_inset

, classic methods to determine distortion will fail to accurately predict
 the performance of wideband electronic circuits under modulated excitation
 signals 
\begin_inset CommandInset citation
LatexCommand cite
key "DeLocht2007"

\end_inset

.
 It is therefore important to use the correct, modulated signals to get
 an accurate representation of the non-linear distortion introduced by the
 circuit.
\end_layout

\begin_layout Standard
Using a modulated signal in an already complicated circuit poses severe
 issues to keep the complexity of a distortion analysis in check.
 Some of the classic methods already return several hundreds of distortion
 contributions for a simple circuit under a single-tone excitation 
\begin_inset CommandInset citation
LatexCommand cite
key "Wambacq1998"

\end_inset

.
 Extending these classic methods to describe the distortion under modulated
 signals is a hopeless endeavour, which is why we opted for a completely
 different approach.
\end_layout

\begin_layout Standard
Our approach to analyse the distortion in a circuit under modulated signals
 is based on the 
\begin_inset Flex gls
status open

\begin_layout Plain Layout
BLA
\end_layout

\end_inset


\begin_inset CommandInset citation
LatexCommand cite
key "Pintelon2012"

\end_inset

.
 As its name suggests, the 
\begin_inset Flex gls
status open

\begin_layout Plain Layout
BLA
\end_layout

\end_inset

 is an approximate linear model that best describes the response of a circuit
 or system to signals with a specified 
\begin_inset Flex gls
status open

\begin_layout Plain Layout
PSD
\end_layout

\end_inset

 and 
\begin_inset Flex gls
status open

\begin_layout Plain Layout
PDF
\end_layout

\end_inset

.
 The 
\begin_inset Flex gls
status open

\begin_layout Plain Layout
BLA
\end_layout

\end_inset

 was introduced in chapters
\begin_inset space ~
\end_inset


\begin_inset CommandInset ref
LatexCommand ref
reference "chap:The-BLA"

\end_inset

 and 
\begin_inset CommandInset ref
LatexCommand ref
reference "chap:BLA_of_circuits"

\end_inset

.
\end_layout

\begin_layout Standard
The 
\begin_inset Flex gls
status open

\begin_layout Plain Layout
BLA
\end_layout

\end_inset

 requires that the underlying system is a 
\begin_inset Flex gls
status open

\begin_layout Plain Layout
PISPO
\end_layout

\end_inset

 system, but does not require it to be weakly non-linear.
 Hence, the results in this thesis are valid for both weakly and strongly
 non-linear circuits.
 To determine the 
\begin_inset Flex gls
status open

\begin_layout Plain Layout
BLA
\end_layout

\end_inset

 of a circuit or system in simulations, multisine signals are used instead
 of the real modulated signals.
 The multisines are shaped such that they have the same 
\begin_inset Flex gls
status open

\begin_layout Plain Layout
PSD
\end_layout

\end_inset

 and 
\begin_inset Flex gls
status open

\begin_layout Plain Layout
PDF
\end_layout

\end_inset

 as the modulated signals the circuit will encounter in its applications.
 Hence, the 
\begin_inset Flex gls
status open

\begin_layout Plain Layout
BLA
\end_layout

\end_inset

 and distortion obtained under multisine excitation will be a correct representa
tion of the real-world performance of the circuit.
 The techniques to obtain the steady-state response of an electronic circuit
 to a multisine excitation were summarised in Appendix
\begin_inset space ~
\end_inset


\begin_inset CommandInset ref
LatexCommand ref
reference "chap:App1_simulatorsettings"

\end_inset

.
\end_layout

\begin_layout Standard
In the 
\begin_inset Flex gls
status open

\begin_layout Plain Layout
BLA
\end_layout

\end_inset

 framework, the distortion is defined as 
\begin_inset Quotes eld
\end_inset

everything in the output not explained by the 
\begin_inset Flex gls
status open

\begin_layout Plain Layout
BLA
\end_layout

\end_inset

".
 This distortion has noise-like properties when a modulated signal is used
 to excite the system.
 The distortion is then zero-mean and uncorrelated to the input signal.
 The power spectrum of the distortion added by the system is a smooth function
 of frequency.
 We have used these properties to combine the 
\begin_inset Flex gls
status open

\begin_layout Plain Layout
BLA
\end_layout

\end_inset

 with a noise analysis to calculate the distortion contributions in a circuit.
\end_layout

\begin_layout Standard
In chapter
\begin_inset space ~
\end_inset


\begin_inset CommandInset ref
LatexCommand ref
reference "chap:DCA"

\end_inset

, we looked into the details of the 
\begin_inset Flex gls
status open

\begin_layout Plain Layout
BLA
\end_layout

\end_inset

-based 
\begin_inset Flex gls
status open

\begin_layout Plain Layout
DCA
\end_layout

\end_inset

.
 We have shown that the distortion contributions in a circuit or system
 can be obtained with a three step procedure:
\end_layout

\begin_layout Enumerate
Determine the 
\begin_inset Flex gls
status open

\begin_layout Plain Layout
BLA
\end_layout

\end_inset

 of each sub-circuit.
\end_layout

\begin_layout Enumerate
Calculate the covariance matrix of all distortion sources at the output
 of the sub-circuits.
\end_layout

\begin_layout Enumerate
Refer each of the distortion contributions to the output of the total circuit.
\end_layout

\begin_layout Standard
The distortion contributions predict the contribution of each sub-circuit
 to the total distortion power seen at the output of the circuit.
 All contributions are real numbers.
 The distortion contributions were split in two classes: direct contributions,
 due to every sub-circuit, and correlation contributions, due to interaction
 between the distortion introduced by two sub-circuits.
 The direct contributions are always positive contributions to the total
 distortion power at the output of the circuit.
 The correlation contributions can be either positive or negative.
 A negative correlation contribution indicates that the distortion generated
 by one sub-circuit is cancelled by another.
 We found that it is very important to keep the correlation among the different
 distortion sources into account in the 
\begin_inset Flex gls
status open

\begin_layout Plain Layout
BLA
\end_layout

\end_inset

-based 
\begin_inset Flex gls
status open

\begin_layout Plain Layout
DCA
\end_layout

\end_inset

 to obtain a correct result.
 
\end_layout

\begin_layout Standard
We applied the 
\begin_inset Flex gls
status open

\begin_layout Plain Layout
BLA
\end_layout

\end_inset

-based 
\begin_inset Flex gls
status open

\begin_layout Plain Layout
DCA
\end_layout

\end_inset

 to determine the distortion contributions in several examples such as a
 low-frequency amplifier, a Doherty Power amplifier and a Gm-C biquad.
 Both weakly and strongly non-linear circuits were analysed.
 Some of the main advantages of the 
\begin_inset Flex gls
status open

\begin_layout Plain Layout
BLA
\end_layout

\end_inset

-based that were demonstrated are the following
\end_layout

\begin_layout Itemize
\begin_inset Argument item:1
status open

\begin_layout Plain Layout
-
\end_layout

\end_inset

The method can be applied hierarchically.
 When a sub-circuit consists of another set of sub-circuits, the 
\begin_inset Flex gls
status open

\begin_layout Plain Layout
BLA
\end_layout

\end_inset

-based 
\begin_inset Flex gls
status open

\begin_layout Plain Layout
DCA
\end_layout

\end_inset

 can be used to zoom in on the sub-circuit.
 This allows the designer to limit the amount of distortion contributions
 in the circuit to a tractable amount.
\end_layout

\begin_layout Itemize
\begin_inset Argument item:1
status open

\begin_layout Plain Layout
-
\end_layout

\end_inset

No special model or simulator access is required for the method to work.
 The 
\begin_inset Flex gls
status open

\begin_layout Plain Layout
BLA
\end_layout

\end_inset

-based 
\begin_inset Flex gls
status open

\begin_layout Plain Layout
DCA
\end_layout

\end_inset

 can be used in a commercial simulator.
 
\end_layout

\begin_layout Itemize
\begin_inset Argument item:1
status open

\begin_layout Plain Layout
-
\end_layout

\end_inset

The 
\begin_inset Flex gls
status open

\begin_layout Plain Layout
BLA
\end_layout

\end_inset

-based 
\begin_inset Flex gls
status open

\begin_layout Plain Layout
DCA
\end_layout

\end_inset

 uses linear models and noise, two concepts designers are familiar with.
\end_layout

\begin_layout Itemize
\begin_inset Argument item:1
status open

\begin_layout Plain Layout
-
\end_layout

\end_inset

The user of the 
\begin_inset Flex gls
status open

\begin_layout Plain Layout
BLA
\end_layout

\end_inset

-based 
\begin_inset Flex gls
status open

\begin_layout Plain Layout
DCA
\end_layout

\end_inset

 is forced to work with the correct random excitation signals as it is impossibl
e to define the 
\begin_inset Flex gls
status open

\begin_layout Plain Layout
BLA
\end_layout

\end_inset

 for such simple signals as two-tones.
\end_layout

\begin_layout Standard
To be able to perform the 
\begin_inset Flex gls
status open

\begin_layout Plain Layout
BLA
\end_layout

\end_inset

-based 
\begin_inset Flex gls
status open

\begin_layout Plain Layout
DCA
\end_layout

\end_inset

 at the transistor level, we used the 
\begin_inset Formula $\SBLA$
\end_inset

 to describe the behaviour of the sub-circuits.
 This is a 
\begin_inset Flex gls
status open

\begin_layout Plain Layout
MIMO
\end_layout

\end_inset

 representation.
 The 
\begin_inset Flex gls
status open

\begin_layout Plain Layout
BLA
\end_layout

\end_inset

-based 
\begin_inset Flex gls
status open

\begin_layout Plain Layout
DCA
\end_layout

\end_inset

 is naturally extended to work with 
\begin_inset Flex gls
status open

\begin_layout Plain Layout
MIMO
\end_layout

\end_inset

 sub-circuits as was shown in chapter
\begin_inset space ~
\end_inset


\begin_inset CommandInset ref
LatexCommand ref
reference "chap:DCA"

\end_inset

, but estimating the 
\begin_inset Flex gls
status open

\begin_layout Plain Layout
MIMO
\end_layout

\end_inset

 
\begin_inset Flex gls
status open

\begin_layout Plain Layout
BLA
\end_layout

\end_inset

 of an electronic circuit in feedback is not easy.
 In chapter
\begin_inset space ~
\end_inset


\begin_inset CommandInset ref
LatexCommand ref
reference "chap:BLA_of_circuits"

\end_inset

, we explained that, to be able to estimate the 
\begin_inset Flex gls
status open

\begin_layout Plain Layout
MIMO
\end_layout

\end_inset

 
\begin_inset Flex gls
status open

\begin_layout Plain Layout
BLA
\end_layout

\end_inset

, extra excitation signals need to be added to the circuit.
 These tickler signals are then placed on interleaved frequency grids to
 obtain an estimate of the 
\begin_inset Formula $\SBLA$
\end_inset

 of the circuit.
\end_layout

\begin_layout Subsection
Stability analysis
\end_layout

\begin_layout Standard
In the second part of the thesis, we shifted our attention from distortion
 analysis to the local stability analysis of electronic circuits.
 In a local stability analysis, the stability of the steady-state solutions
 of the circuit are investigated.
 These steady-state solutions can easily be obtained in a circuit simulator
 with a DC or 
\begin_inset Flex gls
status open

\begin_layout Plain Layout
HB
\end_layout

\end_inset

 simulation, but there is no guarantee that the obtained solution is stable.
 
\end_layout

\begin_layout Standard
The method analyses the impedance presented by the circuit to a small-signal
 perturbation source attached to the circuit.
 When this impedance has poles in the complex right half-plane, the circuit
 is unstable.
 State-of-the-art methods use a rational approximation to determine whether
 the impedance has poles in the right half-plane.
 These rational approximations work fine for the stability analysis of the
 DC solutions of lumped circuits, but they struggle to return reliable results
 in the case of circuits with transmission lines or in large-signal stability
 analysis.
 
\end_layout

\begin_layout Standard
We introduced a stability analysis method that avoids the use of a rational
 approximation.
 Instead, the impedance of the circuit is projected onto a basis of both
 stable and unstable functions.
 Stability analysis then boils down to checking whether any part of the
 impedance is projected onto the unstable basis functions.
 We showed that, when the impedance is transformed to the unit circle, the
 projection on the stable/unstable basis can be calculated with the 
\begin_inset Flex gls
status open

\begin_layout Plain Layout
FFT
\end_layout

\end_inset

, which makes the whole analysis very fast.
 
\end_layout

\begin_layout Standard
The projection-based stability analysis was successfully applied to determine
 the stability of several example circuits.
 The proposed method overcomes many of the issues encountered by the stability
 analysis based on rational approximations.
\end_layout

\begin_layout Standard
\begin_inset Newpage newpage
\end_inset


\end_layout

\begin_layout Section*
Proposals for future work
\end_layout

\begin_layout Standard
The proposed methods still have a lot of opportunities for improvement.
 Speed is the main issue here.
\end_layout

\begin_layout Subsection
Speed-up the estimation of the 
\begin_inset Flex gls
status open

\begin_layout Plain Layout
BLA
\end_layout

\end_inset


\end_layout

\begin_layout Standard
In the current implementation of the 
\begin_inset Flex gls
status open

\begin_layout Plain Layout
DCA
\end_layout

\end_inset

, a very large amount of time is spent acquiring enough phase realisations
 of the multisine(s) to obtain a good estimate of the 
\begin_inset Flex gls
status open

\begin_layout Plain Layout
BLA
\end_layout

\end_inset

 of the different sub circuits.
 There are many different techniques from system identification that can
 be used to reduce the amount of needed phase realisations.
 
\end_layout

\begin_layout Description
Local
\begin_inset space ~
\end_inset

modelling The first possible improvement is to use the 
\begin_inset Flex gls
status open

\begin_layout Plain Layout
LPM
\end_layout

\end_inset


\begin_inset CommandInset citation
LatexCommand cite
key "Pintelon2010,Pintelon2010b"

\end_inset

.
 The 
\begin_inset Flex gls
status open

\begin_layout Plain Layout
LPM
\end_layout

\end_inset

 uses the fact that the 
\begin_inset Flex gls
status open

\begin_layout Plain Layout
BLA
\end_layout

\end_inset

 is a smooth function of frequency to improve the quality of the estimate
 by fitting a local model in each of the frequency points.
 
\end_layout

\begin_layout Description
Global
\begin_inset space ~
\end_inset

modelling A second possibility is to go beyond local models and estimate
 a rational approximation on the complete obtained frequency response.
 This will reduce the variance on the 
\begin_inset Flex gls
status open

\begin_layout Plain Layout
BLA
\end_layout

\end_inset

-estimate when it is done correctly.
 However, the model selection and validation will be a major issue.
\end_layout

\begin_layout Standard
Adding any of these methods to the 
\begin_inset Flex gls
status open

\begin_layout Plain Layout
DCA
\end_layout

\end_inset

 will increase the amount of user intervention that is needed to perform
 the 
\begin_inset Flex gls
status open

\begin_layout Plain Layout
DCA
\end_layout

\end_inset

.
 The reason why these advanced techniques were not used was to demonstrate
 the bare minimum of user interaction needed to perform a 
\begin_inset Flex gls
status open

\begin_layout Plain Layout
BLA
\end_layout

\end_inset

-based 
\begin_inset Flex gls
status open

\begin_layout Plain Layout
DCA
\end_layout

\end_inset

.
\end_layout

\begin_layout Subsection
Study what can be gained with model access
\end_layout

\begin_layout Standard
Throughout the thesis, it was assumed that no access to the internals of
 the transistor models was available.
 Although this is usually the case, it could be very interesting to investigate
 how the 
\begin_inset Flex gls
status open

\begin_layout Plain Layout
BLA
\end_layout

\end_inset

-based 
\begin_inset Flex gls
status open

\begin_layout Plain Layout
DCA
\end_layout

\end_inset

 can be used with full access to the transistor models.
\end_layout

\begin_layout Standard
With access to the transistor models, the 
\begin_inset Flex gls
status open

\begin_layout Plain Layout
DCA
\end_layout

\end_inset

 could be used to predict which non-linearity inside of the transistor is
 adding the most non-linear distortion.
 Such information will only be useful in circuits that consist only of a
 handful of transistors, like 
\begin_inset Flex glspl
status open

\begin_layout Plain Layout
PA
\end_layout

\end_inset

, because the amount of distortion contributions will rise very quickly
 in more complicated circuits.
 In 
\begin_inset CommandInset citation
LatexCommand cite
key "Aikio2011"

\end_inset

, it is shown that such information can be used by a designer to improver
 the linearity of a 
\begin_inset Flex gls
status open

\begin_layout Plain Layout
PA
\end_layout

\end_inset

.
\end_layout

\begin_layout Standard
An additional benefit of having full access to the transistor model is that
 the intrinsic device in the transistor often contains a perfect voltage
 or current controlled source.
 Estimating the 
\begin_inset Flex gls
status open

\begin_layout Plain Layout
BLA
\end_layout

\end_inset

 such a perfect source will be easier than estimating the 
\begin_inset Formula $\SBLA$
\end_inset

 of the be full transistor.
 The estimation step in the 
\begin_inset Flex gls
status open

\begin_layout Plain Layout
DCA
\end_layout

\end_inset

 could be heavily simplified with model access.
 
\begin_inset Flex gls
status open

\begin_layout Plain Layout
MIMO
\end_layout

\end_inset

 identification will still be required, but the problem of choosing the
 location for the tickler tones and determining their amplitude would be
 less outspoken.
\end_layout

\begin_layout Subsection
Extend the representation for circuits with frequency translation 
\end_layout

\begin_layout Standard
Throughout this thesis, we have always worked with circuits that have their
 input and output signals in the same frequency band.
 We have not studied circuits which are designed for frequency translation
 such as mixers.
 To determine the distortion contributions in a full transceiver, we need
 to be able to analyse mixers as well.
\end_layout

\begin_layout Standard
The 
\begin_inset Flex gls
status open

\begin_layout Plain Layout
BLA
\end_layout

\end_inset

 with frequency translation has been studied before.
 For mixers, the 
\begin_inset Flex gls
status open

\begin_layout Plain Layout
BMA
\end_layout

\end_inset

 has been developed 
\begin_inset CommandInset citation
LatexCommand cite
key "Vandermot2007,Vandermot2010"

\end_inset

.
 Incorporating the 
\begin_inset Flex gls
status open

\begin_layout Plain Layout
BMA
\end_layout

\end_inset

 into the existing 
\begin_inset Flex gls
status open

\begin_layout Plain Layout
BLA
\end_layout

\end_inset

-based 
\begin_inset Flex gls
status open

\begin_layout Plain Layout
DCA
\end_layout

\end_inset

 on the system level will not pose many issues.
 Including the 
\begin_inset Flex gls
status open

\begin_layout Plain Layout
BMA
\end_layout

\end_inset

 will be more difficult on the circuit level, because the 
\begin_inset Flex gls
status open

\begin_layout Plain Layout
MIMO
\end_layout

\end_inset

 extension of the 
\begin_inset Flex gls
status open

\begin_layout Plain Layout
BMA
\end_layout

\end_inset

 has not been developed yet.
\end_layout

\begin_layout Standard
The 
\begin_inset Flex gls
status open

\begin_layout Plain Layout
BLA
\end_layout

\end_inset

 with frequency translation will also improve the 
\begin_inset Flex gls
status open

\begin_layout Plain Layout
DCA
\end_layout

\end_inset

 in 
\begin_inset Flex glspl
status open

\begin_layout Plain Layout
PA
\end_layout

\end_inset

.
 By adding the 
\begin_inset Flex gls
status open

\begin_layout Plain Layout
BMA
\end_layout

\end_inset

, the distortion contributions due to the low-frequency dynamics in the
 power supply can be separated from the high-frequency non-linear distortion
 contributions.
 Preliminary research has been performed in that direction 
\begin_inset CommandInset citation
LatexCommand cite
key "Thorsell2011a,Thorsell2011b"

\end_inset

, but again, the 
\begin_inset Flex gls
status open

\begin_layout Plain Layout
MIMO
\end_layout

\end_inset

 extension of the proposed 
\begin_inset Flex gls
status open

\begin_layout Plain Layout
BLA
\end_layout

\end_inset

extension has not been developed yet.
\end_layout

\begin_layout Subsection
Extending the methods to mixed-signal applications
\end_layout

\begin_layout Standard
Besides systems with frequency translation, the second large class of circuits
 we cannot analyse yet with our 
\begin_inset Flex gls
status open

\begin_layout Plain Layout
BMA
\end_layout

\end_inset

-based 
\begin_inset Flex gls
status open

\begin_layout Plain Layout
DCA
\end_layout

\end_inset

 are circuits which have their signals in both the digital and analog domain.
 With an extension to work in both the continuous- and discrete-time domains,
 the distortion introduced by 
\begin_inset Flex glspl
status open

\begin_layout Plain Layout
ADC
\end_layout

\end_inset

 and 
\begin_inset Flex glspl
status open

\begin_layout Plain Layout
DAC
\end_layout

\end_inset

 can be analysed.
 
\begin_inset CommandInset citation
LatexCommand cite
key "Bos2008"

\end_inset

 and 
\begin_inset CommandInset citation
LatexCommand cite
key "Vandersteen2009a"

\end_inset

 already show that the extension to mixed-signal applications is possible,
 but the details of the mixed-signal 
\begin_inset Flex gls
status open

\begin_layout Plain Layout
DCA
\end_layout

\end_inset

, like the aliasing, will need to be looked into.
\end_layout

\begin_layout Standard
Besides 
\begin_inset Flex glspl
status open

\begin_layout Plain Layout
ADC
\end_layout

\end_inset

 and 
\begin_inset Flex glspl
status open

\begin_layout Plain Layout
DAC
\end_layout

\end_inset

, the mixed-signal 
\begin_inset Flex gls
status open

\begin_layout Plain Layout
BLA
\end_layout

\end_inset

-based 
\begin_inset Flex gls
status open

\begin_layout Plain Layout
DCA
\end_layout

\end_inset

 could be used in the analysis of switched-capacitor circuits and in the
 analysis of pre-distorted power amplifiers.
 With a mixed-signal 
\begin_inset Flex gls
status open

\begin_layout Plain Layout
DCA
\end_layout

\end_inset

, the analysis of many low-frequency control systems becomes possible.
 Applications that come to mind where non-linear distortion is critical
 are switching audio power amplifiers 
\begin_inset CommandInset citation
LatexCommand cite
key "Putzeys2003"

\end_inset

 and high-precision motor drives 
\begin_inset CommandInset citation
LatexCommand cite
key "Mohan2003"

\end_inset

.
\end_layout

\begin_layout Subsection
Improvements to the stability analysis
\end_layout

\begin_layout Standard
The final set of improvements that could be added to the work in this thesis
 are related to the stability analysis.
 The projection based approach is perfectly suited to detect an instability
 and to locate the unstable poles, but we have not looked into how this
 information could be used efficiently by the designer.
 
\end_layout

\begin_layout Standard
When an instability is encountered in the circuit, the designer wants to
 know what part of the circuit causes the unstable behaviour and how to
 resolve the problem.
 By analysing several different locations in the circuit, the location of
 the instability could be determined 
\begin_inset CommandInset citation
LatexCommand cite
key "Mori2016"

\end_inset

 and possibly resolved 
\begin_inset CommandInset citation
LatexCommand cite
key "Ayllon2011"

\end_inset

.
\end_layout

\begin_layout Standard
When the circuit is stable, the current implementation of the method is
 not able to provide any information on how far from instability the circuit
 is located.
 By estimating the poles of the stable part, some of that information could
 be obtained.
 Estimating the poles of the stable part is a very difficult task however,
 due to the distributed elements present in the circuits.
 
\end_layout

\begin_layout Standard
When the stable and unstable poles of the circuit can be obtained, the stability
 as a function of one or more circuit parameters could be investigated by
 determining the trajectories of the circuit poles in the complex plane.
 The pole trajectories as a function of the amplitude of the excitation
 signal in a large-signal stability analysis are commonly used.
 Another interesting problem is to investigate the stability of a circuit
 a function of its load impedances.
\end_layout

\end_body
\end_document
