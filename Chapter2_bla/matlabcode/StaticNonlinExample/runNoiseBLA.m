function [ BLA , varBLA , freq ] = runNoiseBLA( rms , M , P , plotBool)

if ~exist('plotBool','var')
    plotBool = false;
end

%%  generate the FIR filter that will band-limit the input signal
Fp  = 0.08;
Fst = 0.12; % Fc = (Fp+Fst)/2;  Transition Width = Fst - Fp
Ap  = 0.06;
Ast = 80;
Hf = fdesign.lowpass('N,Fc',10,0.4);
setspecs(Hf,'Fp,Fst,Ap,Ast',Fp,Fst,Ap,Ast);
Hd = design(Hf,'equiripple','SystemObject',true);
FIR = Hd.Numerator;

%% generate the input signal for the system

% generate some filtered gaussian noise data
e = randn(M,P);
for mm=1:M
    if mm==1
        t = conv(e(mm,:),FIR);
        x = zeros(M,length(t));
        x(mm,:) = t;
    else
        x(mm,:) = conv(e(mm,:),FIR);
    end
end
% remove the filter transient
x = x(:,length(FIR):end-length(FIR));

% recalculate P
P = size(x,2);

% calculate the RMS of the input signal
RMS = sqrt(mean(mean(x.^2)));

% scale the signal such that the rms is correct
x = x/RMS*rms;

% add a mini dc offset to x
x = x + 0.05;


% calculate the spectrum of x
Xspec = fft(x(1,:).*hann(size(x,2)).')/size(x,2);
% only save the positive frequencies
Xspec = Xspec(1:end/2);
% generate the frequency axis
freq = (0:length(Xspec)-1)/length(Xspec)/((Fp+Fst)/2);
if plotBool
    % plot the signal
    figure(163542)
    clf
    subplot(131)
    [counts,bins] = hist(vec(x),50);
    h=barh(bins,counts);
    set(get(h,'Parent'),'xdir','r')
    subplot(132)
    plot(linspace(0,1,length(x(1,:))),x(1,:))
    subplot(133)
    plot(freq,db(Xspec),'k.','MarkerSize',1)
end

%% send the signal through the non-linearity

y = NL(x);

if plotBool
    % calculate the spectrum of y
    Yspec = fft(y(1,:).*hann(size(y,2)).')/size(y,2);
    % only save the positive frequencies
    Yspec = Yspec(1:end/2);
    % plot the output signal
    figure(584654)
    clf
    subplot(131)
    [counts,bins] = hist(vec(y),50);
    h=barh(bins,counts);
    set(get(h,'Parent'),'xdir','r')
    subplot(132)
    plot(linspace(0,1,length(y(1,:))),y(1,:))
    subplot(133)
    plot(freq,db(Yspec),'k.','MarkerSize',1)
end



% remove the mean value of y
y = y-repmat(mean(y,2),[1,P]);
% and remove the DC offset from x
x = x - 0.05;


%% determine the BLA

% calculate the window the diff window to the data
[window, scale] = TheWindow(P, 'mvar', 10);

% calculate the spectrum of the signals
X = fft(x.*repmat(window,[M,1]),[],2);
Y = fft(y.*repmat(window,[M,1]),[],2);

% rng is the range over which the BLA will be calculated
rng = 5:500;

% calculate the cross and autospectra
Syy = mean(Y(:,rng).*conj(Y(:,rng)),1);
Sxx = mean(X(:,rng).*conj(X(:,rng)),1);
Sxy = mean(Y(:,rng).*conj(X(:,rng)),1);


% calculate the BLA
BLA = Sxy./Sxx;

% calculate the variance on the BLA
varY = M/(2*(M-1))*(Syy-abs(Sxy).^2./Sxx);
varBLA = 2*varY/M./Sxx;


freq = freq(rng);

end

